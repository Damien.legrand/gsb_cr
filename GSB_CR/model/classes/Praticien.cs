﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.model.classes
{
	public class Praticien
	{
		  public int PRA_NUM;
		  public string PRA_NOM;
		  public string PRA_PRENOM;

		public Praticien(string pre_nom, string pra_prenom)
		{
			PRA_NOM = pre_nom;
			PRA_PRENOM = pra_prenom;
		}
		public Praticien(int pra_num, string pre_nom, string pra_prenom)
		{
			PRA_NUM = pra_num;
			PRA_NOM = pre_nom;
			PRA_PRENOM = pra_prenom;
		}

		public Praticien()
		{
			
		}
		public Praticien(int pra_num)
		{
			PRA_NUM = pra_num;
		}

		public override string ToString()
		{
			return PRA_NOM + " " + PRA_PRENOM;
		}
	}
}
