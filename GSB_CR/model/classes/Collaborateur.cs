﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.model.classes
{
	public class Collaborateur
	{
		public string COL_MATRICULE;
		public string COL_NOM;
		public string COL_PRENOM;
		public string COL_ADRESSE;
		public string COL_CP;
		public string COL_VILLE;
		public DateTime COL_DATE_EMBAUCHE;
		public string COL_LOGIN;
		public string COL_PASS;
		public string SEC_CODE;
		public string LAB_CODE; 

		public Collaborateur(string col_MATRICULE,string col_NOM, string col_PRENOM,string col_ADRESSE, string col_CP, 
							string col_VILLE,DateTime col_DATE_EMBAUCHE,	string col_LOGIN, string col_PASS, string sec_CODE, string lab_CODE)
		{
			COL_MATRICULE = col_MATRICULE;
			COL_NOM = col_NOM;
			COL_PRENOM = col_PRENOM;
			 COL_ADRESSE = col_ADRESSE;
			 COL_CP = col_CP;
			 COL_VILLE = col_VILLE;
			 COL_DATE_EMBAUCHE = col_DATE_EMBAUCHE;
			COL_LOGIN = col_LOGIN;
			 COL_PASS = col_PASS;
			 SEC_CODE = sec_CODE;
			 LAB_CODE = lab_CODE;
		}

		public Collaborateur()
		{

		}

		public override string ToString()
		{
			return COL_NOM + " " + COL_PRENOM;
		}
	}
}
