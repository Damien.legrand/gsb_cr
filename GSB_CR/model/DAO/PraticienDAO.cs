﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.model.classes;
using GSB_CR.model.DAL;


namespace GSB_CR.model.DAO
{
	class PraticienDAO : DeclarationsDAO
	{
		public List<Praticien> SelectPraticiens()
		{
			List<Praticien> praticiens = new List<Praticien>();
			try
			{

				connection.Open();

				MySqlCommand cmd = this.connection.CreateCommand();

				//requ�te(s)
				cmd.CommandText = "SELECT PRA_NOM, PRA_PRENOM " +
				"FROM PRATICIEN;";
				


				MySqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{

					string n = reader.GetString("PRA_NOM");
					string p = reader.GetString("PRA_PRENOM");

					Praticien praticien = new Praticien(n,p);

					
					praticien.PRA_NOM = n;
					praticien.PRA_PRENOM = p;


					praticiens.Add(praticien);
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				Console.WriteLine("fail" + ex.Message);
			}
			return praticiens;
		}

		public Praticien SelectNumPraticien(Praticien praticien)
		{

			Praticien res = new Praticien();
			
			try
			{
				connection.Open();

				MySqlCommand cmd = this.connection.CreateCommand();

				//requ�te(s)
				cmd.CommandText = " SELECT PRA_NUM FROM PRATICIEN WHERE PRA_NOM=@nom AND PRA_PRENOM=@pre;";

				cmd.Prepare();

				cmd.Parameters.AddWithValue("@nom", praticien.PRA_NOM);
				cmd.Parameters.AddWithValue("@pre", praticien.PRA_PRENOM);

				MySqlDataReader reader = cmd.ExecuteReader();
				int value = 0;
				while (reader.Read())
				{
					value = reader.GetInt32("PRA_NUM");
				}

				res.PRA_NUM = value;

				connection.Close();

			}
			catch (Exception ex)
			{
				Console.WriteLine("fail" + ex.Message);
			}
			return res;
		}

	}
}
