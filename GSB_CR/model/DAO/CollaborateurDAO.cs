﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.model.classes;
using GSB_CR.model.DAL;

namespace GSB_CR.model.DAO
{
	class CollaborateurDAO : DeclarationsDAO
	{
		public List<Collaborateur> SelectCollabos()
		{
			List<Collaborateur> collaborateurs = new List<Collaborateur>();
			try
			{

				connection.Open();

				MySqlCommand cmd = this.connection.CreateCommand();

				//requ�te(s)
				cmd.CommandText = "SELECT COL_MATRICULE FROM COMPTE_RENDU COLLABORATEUR;";

				MySqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					string c = reader.GetString("COL_MATRICULE");

					Collaborateur collaborateur = new Collaborateur();
					collaborateur.COL_MATRICULE = c;

					collaborateurs.Add(collaborateur);
				}
				connection.Close();

			}
			catch (Exception ex)
			{
				Console.WriteLine("fail" + ex.Message);
			}
			return collaborateurs;
		}
	}
}
