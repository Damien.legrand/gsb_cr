﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.model.classes;
using GSB_CR.model.DAO;


namespace GSB_CR.view
{
    public partial class EditCR : Form
    {
        public EditCR()
        {
            InitializeComponent();
        }

		public CompteRendu compteRendu { get; set; }

        private void EditCR_Load(object sender, EventArgs e)
        {
			PraticienDAO praticienDAO = new PraticienDAO();
			List<Praticien> praticiens = new List<Praticien>();
			praticiens = praticienDAO.SelectPraticiens();
			comboBoxPRA.DataSource = praticiens;

			if(compteRendu != null)
			{
				Praticien praticien = new Praticien( compteRendu.Praticien.PRA_NOM,compteRendu.Praticien.PRA_PRENOM);
				Praticien num_pra = new Praticien();
				num_pra = praticienDAO.SelectNumPraticien(praticien);
				txtMatricule.Text = compteRendu.COL_MATRICULE;
				num.Value = compteRendu.RAP_NUM;
				comboBoxPRA.SelectedIndex= num_pra.PRA_NUM;
				dateTime.Value = compteRendu.RAP_DATE;
				txt_bilan.Text = compteRendu.RAP_BILAN;
				txt_motif.Text = compteRendu.RAP_MOTIF;
			}

		}

		private void btnvalid_Click(object sender, EventArgs e)
		{

			bool verif;
			if (compteRendu != null)
			{
				PraticienDAO praticienDAO = new PraticienDAO();

				string praticien;
				praticien = comboBoxPRA.Text;

				string[] table = praticien.Split(' ');

				Praticien obj_praticien = new Praticien(table[0], table[1]);

				Praticien num_pra = praticienDAO.SelectNumPraticien(obj_praticien);

				CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
				CompteRendu compteRendu = new CompteRendu(txtMatricule.Text, num.Value, num_pra.PRA_NUM, dateTime.Value, txt_bilan.Text, txt_motif.Text);

				verif = compteRenduDAO.ModifierCompteRendu(compteRendu);
			}
			else
			{
				PraticienDAO praticienDAO = new PraticienDAO();

				string praticien;
				praticien = comboBoxPRA.Text;

				string[] table = praticien.Split(' ');

				Praticien obj_praticien = new Praticien(table[0], table[1]);

				Praticien num_pra = praticienDAO.SelectNumPraticien(obj_praticien);

				CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
				CompteRendu compteRendu = new CompteRendu(txtMatricule.Text, num.Value, num_pra.PRA_NUM, dateTime.Value, txt_bilan.Text, txt_motif.Text);

				verif = compteRenduDAO.InsertCompteRendu(compteRendu);
			}

			if (verif == true)
			{
				MessageBox.Show("Importation réussi", "My Application", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
			}
			else
			{
				MessageBox.Show("echec de l'insertion", "My Application", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
			}
		

		}

		private void label1_Click(object sender, EventArgs e)
		{

		}

		
	}
}
