﻿namespace GSB_CR.view
{
    partial class EditCR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lbmatricle = new System.Windows.Forms.Label();
			this.txtMatricule = new System.Windows.Forms.TextBox();
			this.lbpra = new System.Windows.Forms.Label();
			this.lbdate = new System.Windows.Forms.Label();
			this.dateTime = new System.Windows.Forms.DateTimePicker();
			this.lbBilan = new System.Windows.Forms.Label();
			this.txt_bilan = new System.Windows.Forms.TextBox();
			this.lbmotif = new System.Windows.Forms.Label();
			this.txt_motif = new System.Windows.Forms.TextBox();
			this.comboBoxPRA = new System.Windows.Forms.ComboBox();
			this.btnvalid = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.num = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.num)).BeginInit();
			this.SuspendLayout();
			// 
			// lbmatricle
			// 
			this.lbmatricle.AutoSize = true;
			this.lbmatricle.Location = new System.Drawing.Point(12, 9);
			this.lbmatricle.Name = "lbmatricle";
			this.lbmatricle.Size = new System.Drawing.Size(58, 13);
			this.lbmatricle.TabIndex = 0;
			this.lbmatricle.Text = "matricule : ";
			// 
			// txtMatricule
			// 
			this.txtMatricule.Location = new System.Drawing.Point(138, 6);
			this.txtMatricule.Name = "txtMatricule";
			this.txtMatricule.Size = new System.Drawing.Size(133, 20);
			this.txtMatricule.TabIndex = 1;
			// 
			// lbpra
			// 
			this.lbpra.AutoSize = true;
			this.lbpra.Location = new System.Drawing.Point(12, 32);
			this.lbpra.Name = "lbpra";
			this.lbpra.Size = new System.Drawing.Size(57, 13);
			this.lbpra.TabIndex = 2;
			this.lbpra.Text = "Praticien  :";
			// 
			// lbdate
			// 
			this.lbdate.AutoSize = true;
			this.lbdate.Location = new System.Drawing.Point(12, 68);
			this.lbdate.Name = "lbdate";
			this.lbdate.Size = new System.Drawing.Size(36, 13);
			this.lbdate.TabIndex = 4;
			this.lbdate.Text = "Date :";
			// 
			// dateTime
			// 
			this.dateTime.Location = new System.Drawing.Point(139, 62);
			this.dateTime.Name = "dateTime";
			this.dateTime.Size = new System.Drawing.Size(221, 20);
			this.dateTime.TabIndex = 5;
			this.dateTime.Value = new System.DateTime(2018, 10, 11, 9, 19, 25, 0);
			// 
			// lbBilan
			// 
			this.lbBilan.AutoSize = true;
			this.lbBilan.Location = new System.Drawing.Point(12, 135);
			this.lbBilan.Name = "lbBilan";
			this.lbBilan.Size = new System.Drawing.Size(36, 13);
			this.lbBilan.TabIndex = 6;
			this.lbBilan.Text = "Bilan :";
			// 
			// txt_bilan
			// 
			this.txt_bilan.Location = new System.Drawing.Point(139, 132);
			this.txt_bilan.Multiline = true;
			this.txt_bilan.Name = "txt_bilan";
			this.txt_bilan.Size = new System.Drawing.Size(222, 328);
			this.txt_bilan.TabIndex = 7;
			// 
			// lbmotif
			// 
			this.lbmotif.AutoSize = true;
			this.lbmotif.Location = new System.Drawing.Point(12, 100);
			this.lbmotif.Name = "lbmotif";
			this.lbmotif.Size = new System.Drawing.Size(36, 13);
			this.lbmotif.TabIndex = 8;
			this.lbmotif.Text = "Motif :";
			// 
			// txt_motif
			// 
			this.txt_motif.Location = new System.Drawing.Point(139, 97);
			this.txt_motif.Name = "txt_motif";
			this.txt_motif.Size = new System.Drawing.Size(221, 20);
			this.txt_motif.TabIndex = 9;
			// 
			// comboBoxPRA
			// 
			this.comboBoxPRA.FormattingEnabled = true;
			this.comboBoxPRA.Location = new System.Drawing.Point(139, 32);
			this.comboBoxPRA.Name = "comboBoxPRA";
			this.comboBoxPRA.Size = new System.Drawing.Size(222, 21);
			this.comboBoxPRA.TabIndex = 10;
			// 
			// btnvalid
			// 
			this.btnvalid.Location = new System.Drawing.Point(15, 391);
			this.btnvalid.Name = "btnvalid";
			this.btnvalid.Size = new System.Drawing.Size(103, 69);
			this.btnvalid.TabIndex = 12;
			this.btnvalid.Text = "Valider";
			this.btnvalid.UseVisualStyleBackColor = true;
			this.btnvalid.Click += new System.EventHandler(this.btnvalid_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(277, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 13;
			this.label1.Text = "Num :";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// num
			// 
			this.num.Location = new System.Drawing.Point(318, 7);
			this.num.Name = "num";
			this.num.Size = new System.Drawing.Size(66, 20);
			this.num.TabIndex = 15;
			// 
			// EditCR
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(396, 472);
			this.Controls.Add(this.num);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnvalid);
			this.Controls.Add(this.comboBoxPRA);
			this.Controls.Add(this.txt_motif);
			this.Controls.Add(this.lbmotif);
			this.Controls.Add(this.txt_bilan);
			this.Controls.Add(this.lbBilan);
			this.Controls.Add(this.dateTime);
			this.Controls.Add(this.lbdate);
			this.Controls.Add(this.lbpra);
			this.Controls.Add(this.txtMatricule);
			this.Controls.Add(this.lbmatricle);
			this.Name = "EditCR";
			this.Text = "EditCR";
			this.Load += new System.EventHandler(this.EditCR_Load);
			((System.ComponentModel.ISupportInitialize)(this.num)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbmatricle;
        private System.Windows.Forms.TextBox txtMatricule;
        private System.Windows.Forms.Label lbpra;
        private System.Windows.Forms.Label lbdate;
        private System.Windows.Forms.DateTimePicker dateTime;
        private System.Windows.Forms.Label lbBilan;
        private System.Windows.Forms.TextBox txt_bilan;
        private System.Windows.Forms.Label lbmotif;
        private System.Windows.Forms.TextBox txt_motif;
		private System.Windows.Forms.ComboBox comboBoxPRA;
		private System.Windows.Forms.Button btnvalid;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown num;
	}
}
